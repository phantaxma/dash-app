**dash-app**
===================

Micro dockerized webapp that allows to select with a selector 2 numerical columns from a table in a PostgresSQL database using [Python Plotly Dash](https://dash.plotly.com/) and [Docker](https://docs.docker.com/).


![Image](./assets/dash-app.png)

Application deployed to AWS Elastic Container can be consulted on the following URL:

[http://dash-publi-1eyb5h4w9u2sx-273814888.eu-west-1.elb.amazonaws.com/](http://dash-publi-1eyb5h4w9u2sx-273814888.eu-west-1.elb.amazonaws.com/)



```
git clone https://gitlab.com/phantaxma/dash-app.git
cd dash-app
```

**To run local**

Into dash-app directory
```
pip install -r requirements.txt
python app.py
```

Copy `env.ini.example` to `env.ini` and add `[database]` credentials. Only PostgresSQL is supported now using `psycopg2`.

If no credentials are configured in `env.ini` by default the app will use `plotly/datasets` [gapminderDataFiveYear](https://raw.githubusercontent.com/plotly/datasets/master/gapminderDataFiveYear.csv).


**If you have docker installed**

Into dash-app directory run the following command:
```
docker composer up
```

Open URL shown in terminal in the browser

Deploy to AWS
====

To deploy the app deploy to [AWS](aws.amazon.com) Elastic Container using [copilot](https://aws.amazon.com/es/containers/copilot/) command line interface.

Before deploy is need it to configure credentials for docker. For linux users: https://docs.docker.com/desktop/get-started/#credentials-management-for-linux-users

```
copilot init
```


