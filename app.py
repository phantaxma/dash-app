from dash import Dash, html, dcc
import plotly.express as px
import pandas as pd
from dash_bootstrap_components.themes import BOOTSTRAP

from src.data.data_access import load_data_as_dataframe
from src.components.two_selector_dropdown import custom_dropdown
from src.components.update_scatter_plot import update_scatter_plot
from src.components.constants import SCATTER_FIGURE_ID

if __name__ == '__main__':

    app = Dash(__name__, external_stylesheets=[BOOTSTRAP])
    app.title = "Dashboard"
    df = load_data_as_dataframe()

    fig = px.scatter(pd.DataFrame(columns=['x', 'y']), x='x', y='y')
    dcc.Graph(
        id=SCATTER_FIGURE_ID,
        figure=fig
    )

    app.layout = html.Div(children=[
        html.H1(app.title),
        html.Hr(),
        custom_dropdown(df.columns),
        dcc.Graph(
            id=SCATTER_FIGURE_ID,
            figure=fig
        )
        # custom_scatter_plot.render(app, df, xlabel=df.columns[2], ylabel=df.columns[3])
    ], className='container')
    
    update_scatter_plot(app)

    app.run_server(host='0.0.0.0',debug=True, port=8050)
