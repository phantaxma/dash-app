# syntax=docker/dockerfile:1
FROM python:3.9-slim-bullseye
LABEL maintainer "Maximo Ramirez <decimo@mailfence.com>"
RUN mkdir /app
WORKDIR /app

#RUN apk add --no-cache gcc g++ musl-dev linux-headers libpq-dev 
RUN apt update 
RUN apt install -y gcc libpq-dev gnupg2 pass

COPY requirements.txt /app/
RUN pip install -r requirements.txt
EXPOSE 8050
COPY . ./app
CMD ["python", "./app/app.py"]
