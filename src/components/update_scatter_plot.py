from dash import Dash, dcc, html, ctx, Input, Output
import plotly.express as px
from src.components.constants import SCATTER_FIGURE_ID, DROP_DOWN_SELECTOR_ID, DROP_DOWN_SWITCH_ID
import pandas as pd
from ..data.data_access import load_data_as_dataframe

def update_scatter_plot(app):
    
    @app.callback(
        Output(SCATTER_FIGURE_ID, 'figure'),
        Output(DROP_DOWN_SELECTOR_ID, "options"),
        Output(DROP_DOWN_SELECTOR_ID, "value"),
        Input(DROP_DOWN_SELECTOR_ID, 'value'),
        Input(DROP_DOWN_SELECTOR_ID, 'options'),
        Input(DROP_DOWN_SWITCH_ID, "n_clicks"),
        # prevent_initial_call=True
    )
    def update_scatter_from_dropdown(x_y_axes, options, switcher):
        
        new_options = options
        fig = None
        triggered_id = ctx.triggered_id

        if  x_y_axes and len(x_y_axes) == 2 and triggered_id == DROP_DOWN_SWITCH_ID:
            x_y_axes = list(reversed(x_y_axes))

        if x_y_axes and len(x_y_axes) >= 2:
            new_options = [
                {"label": option["label"], "value": option["value"], "disabled": True}
                for option in options]

            xaxis_column_name = x_y_axes[0]
            yaxis_column_name = x_y_axes[1]

            data = load_data_as_dataframe()

            fig = px.scatter(data, x=xaxis_column_name, y=yaxis_column_name)
            fig.update_layout(margin={'l': 40, 'b': 40, 't': 10, 'r': 0}, hovermode='closest')

        else:
            fig = px.scatter(pd.DataFrame(data=[], columns=['x', 'y']), x='x', y='y')
            fig.update_layout(margin={'l': 40, 'b': 40, 't': 10, 'r': 0}, hovermode='closest')

            new_options = [
                {"label": option["label"], "value": option["value"], "disabled": False}
                for option in options]

        return fig, new_options, x_y_axes
