from dash import html, dcc
 
from src.components.constants import DROP_DOWN_LABEL, DROP_DOWN_SWITCH_LABEL, DROP_DOWN_SELECTOR_ID, DROP_DOWN_SWITCH_ID

def custom_dropdown(items):
    return html.Div(
        className='row',
        children=[
            html.P(DROP_DOWN_LABEL), 
            html.Div(
                className='col-6',
                children=[
                    dcc.Dropdown(
                    id=DROP_DOWN_SELECTOR_ID,
                    options=[{"label": item, "value": item } for item in items],
                    multi=True,
                    className='form-select')
                ]
            ),
            html.Div(
                className='col-6', 
                children=[
                    html.Button(
                        DROP_DOWN_SWITCH_LABEL,
                        className='btn btn-primary', 
                        id=DROP_DOWN_SWITCH_ID,
                        n_clicks=0
                        )
                ]
            )
        ],
    )
