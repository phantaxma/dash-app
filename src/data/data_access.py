import psycopg2
import pandas as pd
from config.config import get_env_params

def load_data_as_dataframe():

    connection = False
    data = None
    column_names = []

    try:
        params = get_env_params('database', 'env.ini')
        connection = psycopg2.connect(**params)
        cursor = connection.cursor()
        cursor.execute('SELECT * FROM auto;')
        data = cursor.fetchall()

        column_names = [col[0] for col in cursor.description]
    except (Exception, psycopg2.Error) as error:
        print("Error while fetching data from PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            # print("PostgreSQL connection is closed")
            return pd.DataFrame(data,columns=column_names)
        else:
            df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/gapminderDataFiveYear.csv')
            return df
    
