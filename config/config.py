from configparser import ConfigParser
import os

def get_env_params(section, filename='env.ini'):
    parser = ConfigParser()
    parser.read(os.path.join(os.path.dirname(__file__), f'../{filename}'))

    env_params = {}

    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            env_params[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return env_params

